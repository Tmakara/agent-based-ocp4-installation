High Level Scope of Work
------------------------
- > _1. RHCOS boot images creation for all VMs
- > 2. Cluster bootstrap
- > 3. Control plane deployment
- > 4. Add all nodes to the cluster (except external-router nodes) 
- > 5. Chrony and TZ Machine configs
- > 6. Create Machine Config Pools 
- > 7. Label & Taint Nodes 
- > 8. Move router pods to dedicated infra nodes 
- > 9. Installing Local Storage Operator 
- > 10. Create Local Volumes 
- > 11. Monitoring Configuration
- > 12. Logging Deployment 
- > 13. Authentication 
- > 14. Internal Registry Configuration - Test Internal Registry 
- > 15. LDAP Group Sync 
- > 16. Disable self provisioning 
- > 17. Remove kubeadmin user 
- > 18. Configure Egress IPs 
- > 19. Log Forwarder ```
- > 20. External Router deployment
- > 21. ETCD Backup _ 


# Install Openshift 4 in an environment without an internet connection

Red Hat since openshift-install CLI 4.12 has an agent-based install feature, allowing us to deploy openshift more easily in highly customizable environments. It provides a somewhat automatic experience in the installation steps where we only need to prepare infrastructure information such as the IP addresses of the nodes, DNS records,... This guide will help you clearly. Learn more about the steps to take to install Openshift using the agent-based feature of the openshift-install installer. We will also try to install openshift in a disconnected environment. To install Openshift cluster in an environment without internet connection, we need to perform the following steps:

1. Deploy _mirror Rotate the registry_ - we will mirror all the content needed for Red Hat's cloud installation to this local registry
2. If you are using _openshift-install installer version 4.11_ then we need to rebuild this installer to add _agent-based install_ feature
3. Next we need to create an ISO file to install the RHCoreOS operating system on the nodes
4. And finally track the installation progress and do some initial setup (the initial setup you can refer to the clip on youtube)

You can refer to the clip made on the youtube channel [RedHat VN Labs](https://www.youtube.com/@rhvnlabs)

To prepare we need:
- 1 bastion host running RHEL8 with GUI interface with Firefox (4vCPU, 8Gb RAM, 120Gb HDD) - we will interact with openshift through this bastion host. This host will also mirror the registry (in the clip we use the name __quay__ for this host, but below in the tutorial we still leave it as __bastion__ for you to follow easily)
- 3 master nodes (8vCPU, 16Gb RAM, 120Gb HDD) - OS will be installed later
- 2 worker nodes (8vCPU, 16Gb RAM, 120Gb HDD) - OS will be installed later

In addition, we also need a DNS server with optional domain names (in this lab we use Red Hat IdM server, with internal domain vnlabs.dev). In the content of the tutorial, where you use the domain name vnlabs.dev, you can replace it with your optional domain name.

We also assume the username on the bastion host is user (in the youtube clip you see it's thanhnq)

With all the above preparation, the detailed implementation steps are as follows:

## Mirror Deployment Rotate the registry

Before doing so we need to access [https://console.redhat.com](https://console.redhat.com) to download:
- pull secret
- __oc__ command-line tool
- __openshift-install__ command-line tool

Copy downloaded files to bastion host, directory _/home/user_
```
[user@bastion ~]# wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-install-linux.tar.gz 
[user@bastion ~]# wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/openshift-client-linux.tar.gz 
[user@bastion ~]# tar zxf openshift-client-linux.tar.gz
[user@bastion ~]# tar zxf openshift-install-linux.tar.gz
[user@bastion ~]# mkdir /home/user/bin
[user@bastion ~]# mv oc kubectl openshift-install ~/bin
```
create directory _~/mirror_, put pull secret in directory _mirror_ and name its file _pull-secret.txt_
```
[user@bastion ~]# cd mirror
[user@bastion mirror]# sudo yum install -y podman httpd-tools openssl tmux \ 
			net-tools nmstate git golang make zip bind-utils jq
```
Set the directory containing the content for the mirror registry and proceed with the registry installation
```
[user@bastion mirror]# echo 'REGISTRY_PW=xxxxxxxyyyyyyyyyyzzzzzzzzzzz' >> ~/.bashrc
[user@bastion mirror]# source ~/.bashrc
```

(Note that saving password information to the environment file will cause a big security risk, so we only do this in the lab)

Next, update the trusted CA on the bastion host
```
[user@bastion mirror]# sudo cp /quay/quay-rootCA/rootCA* /etc/pki/ca-trust/source/anchors/ -v
[user@bastion mirror]# sudo update-ca-trust extract
```
Login to Registery
```
[user@bastion mirror]# podman login --authfile pull-secret.txt -u init \
                        -p $REGISTRY_PW $REGISTRY_SERVER:8443 --tls-verify=false
```
after this login command, a secret content entry corresponding to the name of the registry mirror will be automatically added to pull-secret.txt, which we can check by viewing the contents of this file.

Next, use _jq_ to convert the pull-secret.txt to _json_ and copy to the necessary paths
```
[user@bastion mirror]# cat ./pull-secret.txt | jq . > pull-secret.json
[user@bastion mirror]# mkdir -p ~/.config/containers
[user@bastion mirror]# cp pull-secret.json ~/.config/containers/auth.json
[user@bastion mirror]# mkdir ~/.docker
[user@bastion mirror]# cp pull-secret.json ~/.docker/config.json
```
Next, we configure the firewall on the bastion host
```
[user@bastion mirror]# sudo firewall-cmd --add-port 8443/tcp --permanent
[user@bastion mirror]# sudo firewall-cmd --add-service dns --permanent
[user@bastion mirror]# sudo firewall-cmd --reload
```
So far we can already use Firefox to access the registry:
```
[user@bastion mirror]# echo $REGISTRY_SERVER 
```
use return value to open connection on Firefox, for example: https://bastion.vnlabs.dev:8443

Next, we install the oc-mirror tool
```
[user@bastion mirror]# curl -L -s -o - https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable/oc-mirror.tar.gz |sudo tar x -C /usr/local/bin -vzf - oc-mirror && sudo chmod +x /usr/local/bin/oc-mirror
```
Run the command below to make sure the installation was successful
```
[user@bastion mirror]# oc mirror help
```
Configure settings to start the mirroring process
```
[user@bastion mirror]# oc mirror init --registry $REGISTRY_SERVER:8443/ocp4/openshift4/mirror/oc-mirror-metadata > imageset-config.yaml
```
After the command is done, we open the _imageset-config.yaml_ file to customize. For example:
- to mirror a specific version (opensshift 4.11.6), we edit the following content:
```
platform:
channels:
- name: stable-4.11
minVersion: 4.11.6
maxVersion: 4.11.6
```
- To add some necessary operators, we edit as follows:
```
operators:
- catalog: registry.redhat.io/redhat/redhat-operator-index:v4.11
packages:
- name: cincinnati-operator
- name: local-storage-operator
  channels:
  - name: stable
- name: odf-operator
- name: kubevirt-hyperconverged
  channels:
  - name: stable
```
(refer to the complete _imageset-config.yaml_ file in this github repo)

To see the list of operators and channels available with openshift 4.11, we run the command:
```
[user@bastion mirror]# oc-mirror list operators --catalog registry.redhat.io/redhat/redhat-operator-index:v4.11
```
then add to the file _imageset-config.yaml_ the necessary operators

Finally, we run the following command to perform mirror
```
[user@bastion mirror]# oc mirror --config=./imageset-config.yaml docker://$REGISTRY_SERVER:8443
```

Note:
- this process will create a directory named __oc-mirror-workspace__ later we will use the content in this directory to display the list of operators added in the imageset-config.yaml file
- The mirroring process will take about 30-60 minutes depending on the number of operators and the quality of the internet connection
- if you encounter an error during mirroring, you can add the __--continue-on-error__ key to the above __oc mirror__ statement to complete the mirroring process

Set environment variables:
```
[user@bastion mirror]# export OPENSHIFT_INSTALL_RELEASE_IMAGE_OVERRIDE=`hostname`:8443/openshift/release-images@<sha256 value>
```
_sha256 value_:
- to get this value, go to your browser and access the mirror registry (eg https://bastion.vnlabs.dev:8443/openshift/release-images)
- go to Tags, in the MANIFEST column, click SHA256, then copy the SHA256 value and paste it into the above command
(see more youtube clips if not clear)

## Setup HAProxy on bastion host (optional)
We can use HAProxy as an external load balancer for our Openshift cluster. In theory, the cluster will need a load balancer for the following two components:
- cluster API endpoint (via port 6443 and 22623)
- ingress endpoint (so that users can access apps inside the cluster via port 80/443)

If we don't use HAProxy then the cluster will use the internal load balancer, however the internal load balancer will have some limitations. Therefore, if possible we should set up an external HAProxy.
In this repo provides a sample haproxy configuration file so that we can customize it accordingly.

Note:
- If the bastion host has SELinux in enforcing mode, we need to execute the following command:
```
[user@bastion mirror]# sudo setsebool -P haproxy_connect_any=1
```
More reference: https://docs.openshift.com/container-platform/4.13/installing/installing_platform_agnostic/installing-platform-agnostic.html#installation-load-balancing-user-infra-example_installing-platform-agnostic

## Build openshift-install tool (optional)
If you download openshift-install version 4.12 then no need to do this step
With version 4.11, because the agent-based installation feature is not supported yet, we have to build it ourselves.

You can test your openshift-install by running _openshift-install_, if under Available Commands there is no __agent__ (agent Commands for supporting cluster installation using agent installer) then you will need to do this build
```
[user@bastion mirror]# ~/bin/openshift-install
Creates OpenShift clusters


Usage:
openshift-install [command]


Available Commands:
analyze Analyze debugging data for a given installation failure
completion Outputs shell completions for the openshift-install command
coreos Commands for operating on CoreOS boot images
create Create part of an OpenShift cluster
destroy Destroy part of an OpenShift cluster
explain List the fields for supported InstallConfig versions
gather Gather debugging data for a given installation failure
graph Outputs the internal dependency graph for installer
help Help about any command
migrate Do a migration
version Print version information
wait-for Wait for install-time events


Flags:
--dir string assets directory (default ".")
-h, --help help for openshift-install
--log-level string log level (e.g. "debug | info | warn | error") (default "info")


Use "openshift-install [command] --help" for more information about a command.
```

To build, copy the file billie-release.sh to the bastion host, chmod it:
```
[user@bastion mirror]# chmod +x billie-release.sh
```

Next we run the following command to build tool, below command build __openshift-install__ tool for __version 4.11.6__
```
[user@bastion mirror]# ./billi-release.sh agent-installer ${OPENSHIFT_INSTALL_RELEASE_IMAGE_OVERRIDE} 4.11.6
```

The build process will take about 45-60', after the build is complete, we try to run the openshift-install command again to see if the agent feature is up:
```
[user@bastion mirror]# ./openshift-install
Creates OpenShift clusters


Usage:
openshift-install [command]


Available Commands:
agent Commands for supporting cluster installation using agent installer
analyze Analyze debugging data for a given installation failure
completion Outputs shell completions for the openshift-install command
coreos Commands for operating on CoreOS boot images
create Create part of an OpenShift cluster
destroy Destroy part of an OpenShift cluster
explain List the fields for supported InstallConfig versions
gather Gather debugging data for a given installation failure
graph Outputs the internal dependency graph for installer
help Help about any command
migrate Do a migration
version Print version information
wait-for Wait for install-time events


Flags:
--dir string assets directory (default ".")
-h, --help help for openshift-install
--log-level string log level (e.g. "debug | info | warn | error") (default "info")


Use "openshift-install [command] --help" for more information about a command.
```

## Create ISO file and Install Openshift

Create a new folder in mirror
```
[user@bastion mirror]# mkdir cluster
```

Generate an ssh key so that the nodes can be easily administered later
```
[user@bastion mirror]# ssh-keygen -t rsa -N '' -f node_ssh_key
```

Create 2 files _install-config.yaml_ and _agent-config.yaml_ in the cluster directory (refer to 2 corresponding files in this repo). These 2 files contain the necessary information to install the cluster such as IP address, MAC, Gateway, DNS server, ...

For the _install-config.yaml_ file:
- _machineNetwork_: IP range for nodes
- _sshKey_: is the content of the file __node_ssh_key.pub__
- _pullSecret_: get the corresponding part in the file pull-secret.json, paste it in json format, for example:
```
pullSecret: '{"bastion.vnlabs.dev:8443": { "auth": "aW5pdDpYOGFxclJwWjJXSDFUOVFpR0NORHZBbjVvMzYwWTc0Uw==" } }'
```
- _additionalTrustBundle_: is the content of the file __/etc/pki/ca-trust/source/anchors/rootCA.pem__ on the bastion host, run the following command to import the contents of rootCA.pem:
```
[user@bastion mirror]# sed -e 's/^/ /' /etc/pki/ca-trust/source/anchors/rootCA.pem >> install-config.yaml
```

Next, we will create a _Machineconfig_ that uses _butane_ to configure chrony for the nodes during the installation process (https://docs.openshift.com/container-platform/4.11/installing/install_config/installing-customizing.html)
Visit https://mirror.openshift.com/pub/openshift-v4/clients/butane/ to check and get the download link of the latest version
Download _butane_ binary:
```[user@bastion mirror]# curl https://mirror.openshift.com/pub/openshift-v4/clients/butane/latest/butane --output butane```

chmod to make the file executable and move it to ~/bin
```
[user@bastion mirror]# chmod +x butane
[user@bastion mirror]# mv butane ~/bin/butane
```

Create file _99-worker-custom.bu_ - note change the IP address below pointing to the NTP server you want to use
```
[user@bastion mirror]# vi cluster/99-worker-custom.bu
variant: openshift
version: 4.11.0
metadata:
   name: 99-worker-custom
   labels:
     machineconfiguration.openshift.io/role: worker
openshift:
   kernel_arguments:
     - loglevel=7
   storage:
     files:
       - path: /etc/chrony.conf
         mode: 0644
         overwrite: true
         contents:
           inline: |
             pool 192.168.67.28 prefer iburst
             pool 192,168,67,254 iburst
             driftfile /var/lib/chrony/drift
             makestep 1.0 3
             rtcsync
             logdir /var/log/chrony
```

Create _Machineconfig_:
```
[user@bastion mirror]# butane cluster/99-worker-custom.bu -o cluster/99-worker-custom.yaml
```

Next, we are ready to create the ISO file, before we do:
- Backup the files in the cluster folder to another location
- we need to __make sure the bastion host can't reach the internet__.
After disconnecting from the internet of bastion, we execute the following command:
```
[user@bastion mirror]# ~/backup && cp cluster/*.yaml ~/backup
[user@bastion mirror]# ~/mirror/openshift-install agent create image --log-level debug --dir cluster
```

After the command is run in the _cluster_ directory, there will be 1 __agent.iso__ file and 1 _auth_ directory created, and 2 install-config.yaml and agent-config.yaml files are automatically deleted. __Note this iso file can only be used for installation within 24 hours after it is created, after that self-signed certificate will expire, you will need phjair to generate new iso file again.__

Next, we will do the following:
- Upload agent.iso file to vmware,
- mount it to pre-made nodes as CD-ROM,
- and Power On the VM up.
The installation of openshift will be done automatically during node boot.

To monitor the installation process, we execute the following command:
```
[user@bastion mirror]# ./openshift-install agent wait-for install-complete --dir cluster
```
this process will take about 60', during which time the nodes will be automatically rebooted several times, and you will probably see some FAILED messages, let alone until the end...
You will be notified of the success or failure of the deployment. If all else fails, you need:
- delete all content in the cluster folder,
- copy the backed up files back into this folder,
- Review the contents of these yaml files to see if they are correct (IP, MAC, Gateway, DNS, sshKey, secret, ...)
- ... and execute the above _openshift-install_ command again to recreate the iso file, mount the CD-ROM and reboot the machines (remember in this reboot the nodes must boot from the CD-ROM first).

## Some initial settings
To be able to use the cluster, we need to set up some of the following configurations:

Set _kubeadmin_ password
```
[user@bastion mirror]# oc patch secret -n kube-system kubeadmin --type json \
   -p '[{"op": "replace", "path": "/data/kubeadmin", "value": "'"$(openssl rand -base64 18 | tee /home/thanhnq/mirror/cluster/auth /kubeadmin-password | htpasswd -nBi -C 10 "" | cut -d: -f2 | base64 -w 0 -)"'"}]'

secret/kubeadmin patched


[user@bastion mirror]# ls -l /home/thanhnq/mirror/cluster/auth/kubeadmin-password
-rw-r-----. 1 bschmaus bschmaus 25 Aug 18 10:50 auth/kubeadmin-password


[user@bastion mirror]# cat auth/kubeadmin-password
<YOUR KUBEADMIN PASSWORD>
```

Next, you need to fix the error related to the certificate, run the command below to check if you have an error or not:
```
[user@bastion ~]# oc login
error: x509: certificate signed by unknown authority
[user@bastion ~]# oc whoami
system:admin
```

The handling on bastion is as follows: https://docs.openshift.com/container-platform/4.11/security/certificate_types_descriptions/ingress-certificates.html
```
[user@bastion ~]# oc get secret router-ca -n openshift-ingress-operator -o json | \
jq -r '.data."tls.crt"' | base64 -d | sudo tee /etc/pki/ca-trust/source/anchors/ingress-root-CA.pem
[user@bastion ~]# oc get secret router-ca -n openshift-ingress-operator -o json | \
jq -r '.data."tls.crt"' | base64 -d | sudo tee /etc/pki/ca-trust/source/anchors/ingress-root-CA.key


[user@bastion ~]# update-ca-trust extract
```

Next, we disable the default OperatorHub
```
[user@bastion ~]# oc patch OperatorHub cluster --type json -p '[{"op": "add", "path": "/spec/disableAllDefaultSources", "value": true}]'
```

Then we will install ImageContentSourcePolicy and CatalogSource resources into the cluster:
```
[user@bastion ~]# oc apply -f ./oc-mirror-workspace/results-1639608409/
[user@bastion ~]# oc get imagecontentsourcepolicy --all-namespaces
[user@bastion ~]# oc get catalogsource --all-namespaces
```

Finally, add the identityProvider to the cluster, for example, we will add the following users and passwords to be able to authenticate to the cluster:
```
[user@bastion ~]# htpasswd -c -B -b ./ocp-user-passwd user1 p@ssw0rd1
[user@bastion ~]# htpasswd -b ./ocp-user-passwd user2 p@ssw0rd2
[user@bastion ~]# oc create secret generic local-idp-secret \
                   --from-file htpasswd=./ocp-user-passwd -n openshift-config
[user@bastion ~]# oc edit oauth cluster
spec:
   identityProviders:
   - htpasswd:
       fileData:
         name: local-idp-secret
     mappingMethod: claim
     name: local-users
     type: HTPasswd
```

Assign permissions to users, for example user1 has cluster-admin rights
```
[user@bastion ~]# oc adm policy add-cluster-role-to-user cluster-admin user1
```

After decentralizing, we can delete the kubeadmin user to reduce security risks (note, only do this after we already have another user with cluster-admin rights)
```
[user@bastion ~]# oc delete secret kubeadmin -n kube-system
```

So we have installed openshift cluster in disconnected environment.

You can watch more youtube clips if you don't know the steps.

Good luck ! 
